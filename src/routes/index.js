import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../pages/Splash';
import Home from '../pages/Home';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
             />
             <Stack.Screen 
                name="Home"
                component={Home}
                options={{headerShown: false}}
             />
        </Stack.Navigator>
    );
};

export default Route;