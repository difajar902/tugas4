import React from 'react';
import {View, StyleSheet, Text, Image, ScrollView} from 'react-native';
import Message from '../components/position';
import User from '../components/position2';
import Friend from '../components/position21';
import Friend2 from '../components/position22';
import Body from '../components/conten1';
import Body2 from '../components/conten2';
import Body3 from '../components/conten3';
import Body4 from '../components/conten4';
import Body5 from '../components/conten5';
import Body6 from '../components/conten6';
import Body7 from '../components/conten7';
import Body8 from '../components/conten8';
import Body9 from '../components/conten9';

const Home = () => {
    return (
      <View style={styles.utama}>
            <View>
                <View style={styles.box}>
                    <Text style={styles.text}>Instagram</Text>
                <View>
                    <Image style={styles.love} source={require('../assets/love1.jpg')}/>
                </View>
                <Message />
                </View>
            </View> 
        <ScrollView>         
            <View style={styles.imp}>
              <User />
              <Friend />
              <Friend2 />
            </View>
            <View>
                <Body />
                <Body2 />
                <Body3 />
                <Body4 />
                <Body5 />
                <Body6 />
                <Body7 />
                <Body8 />
                <Body9 />
            </View>
        </ScrollView>
      </View>

    );
};
const styles = StyleSheet.create({
  utama: {
    backgroundColor: 'white'
  },
  box: {
    padding : 5,
    flexDirection: 'row',
    backgroundColor:'white'
  },
  text: {
    margin: 10,
    fontFamily: 'times new roman',
    fontSize: 25,
    fontWeight: 'bold'
  },
  love: {
    borderRadius: 100,
    marginTop: 3,
    width: 50,
    height: 50,
    marginLeft: 100,
  },
  imp: {
    flexDirection: 'row',
  },
  box1: {
    padding : 2,
    flexDirection: 'row',
    backgroundColor:'white',
    marginLeft: 0,
  },
  
    
})

export default Home;