import React, {useEffect} from 'react';
import {Image, StyleSheet, View, Text} from 'react-native';
import Logo from '../assets/instagram.jpg';

const Splash = ({navigation}) => {
    useEffect (() => {
        setTimeout (() => {
            navigation.replace('Home');
        }, 1000);
    });
    return (
        <View style={styles.container}>
            <Image source={Logo} style={styles.logo} />
            <Text style={styles.text1}>from</Text>
            <Text style={styles.text}>F a c e b o o k</Text>
        </View>
    );
};

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 65,
        height: 65,
        marginTop: 230
    },
    text1: {
        fontSize: 20,
        color: 'gray',
        marginTop: 220
    },
    text: {
        fontSize: 25,
        color: 'red',
    }
});

export default Splash;
